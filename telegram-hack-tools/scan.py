#!/usr/bin/python3
from distutils.archive_util import make_archive
import nmap


class Scanner:
    def __init__ (self, ip, port):
        self.ip = ip
        self.port = port

    def prompt_ip_port (update, context):
        context.bot.send_message (chat_id=update.effective_chat.id, text="Nmap\nEnter ip or hostname and port: ")

    def run_scan (self):
        nm = nmap.PortScanner ()
        nm.scan (str (self.ip), str (self.port))
        res_text = self.make_pretty_output (nm)
        return res_text

    def make_pretty_output (self, nm):
        res_text = ''
        o_port_exist = False
        for host in nm.all_hosts():
            res_text += f"Your target: {self.ip}\n"
            res_text += f"IP: {nm[host]['addresses']['ipv4']}\n"
            res_text += f"Hostname: {nm[host]['hostnames'][0].get('name') or None}\n"
            res_text += f"--------------------------------------------------------\n"
            if nm[host].get ('tcp'):
                for port in nm[host]['tcp']:
                    if nm[host]['tcp'][port]['state'] == "open":
                        res_text += f"\t\t\tPort: {port}  State: {nm[host]['tcp'][port]['state']}\n"
                        o_port_exist = True
                res_text += "" if o_port_exist else "Open port(s) is does not find."
        return res_text