#!/usr/bin/python3
from telegram.ext import *
from telegram import *
from requests import *
from pprint import pprint
import nmap
from scan import Scanner

TOKEN = "871783617:AAFcIkJ-Yytvq7IVxQkBbKAKPdDGoarve7U"
updater = Updater (token=TOKEN)
bot = Bot (token=TOKEN)
dispatcher = updater.dispatcher
flag = ''
keyboard = ['nmap']

def deleteMessage (chat_id, message_id):
    bot.delete_message (chat_id, message_id)

def startCommand (update: Update, context: CallbackContext):
    print ('command handler')
    buttons = []
    buttons.append(KeyboardButton (button) for button in keyboard)
    context.bot.send_message (chat_id=update.effective_chat.id, text="Welcome", reply_markup=ReplyKeyboardMarkup(buttons))

def run (message):
    global flag
    res_text = ''
    if flag in keyboard and flag == 'nmap':
        ip, port = message.split (" ")
        sc = Scanner (ip, port)
        res_text = sc.run_scan ()
    flag = ''
    return res_text

def messageHandler (update:Update, context:CallbackContext):
    global flag
    print ('message handler')
    if update.message.text in keyboard and update.message.text == 'nmap':
        deleteMessage (update.effective_chat.id, update.message.message_id)
        flag = update.message.text
        Scanner.prompt_ip_port (update, context)
    else:
        res_text = run (update.message.text)
        if not res_text == '':
            context.bot.send_message (chat_id=update.effective_chat.id, text=res_text)
        else:
            deleteMessage (update.effective_chat.id, update.message.message_id)


dispatcher.add_handler (CommandHandler ("start", startCommand))
dispatcher.add_handler (MessageHandler (Filters.text, messageHandler))

updater.start_polling ()